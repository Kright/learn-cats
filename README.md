I wrote this code when I read book ["scala with cats"](https://underscore.io/books/scala-with-cats/).

The book contains a lot of small exercises. Exercises are shipped with solutions, but before reading them I tried to write code by myself. 