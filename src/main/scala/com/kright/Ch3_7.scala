package com.kright

import cats.{Contravariant, Monoid, Show}
import cats.instances.string._
import cats.syntax.contravariant._

object Ch3_7 extends App {

	val showString = Show[String]

	//	val showSymbol = Contravariant[Show].contramap(showString)((sym: Symbol) => s"'${sym.name}'")
	//	val showSymbol = showString.contramap((sym: Symbol) => s"'${sym.name}'")
	val showSymbol = showString.contramap[Symbol](s => s"'${s.name}'")

	println(showSymbol.show('wtf))

	{
		import cats.syntax.invariant._
		import cats.syntax.semigroup._

		implicit val symbolMonoid: Monoid[Symbol] =
			Monoid[String].imap(Symbol(_))(_.name)

		println('a |+| 'sym)
	}
}
