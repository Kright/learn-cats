package com.kright

import cats.Semigroup
import cats.data.Validated
import cats.data.Validated.{Invalid, Valid}
import cats.instances.list._
import cats.syntax.semigroup._
import cats.syntax.either._

object Ch10 extends App {

	final case class CheckF[E, A](func: A => Either[E, A]) {
		def apply(a: A): Either[E, A] = func(a)

		def and(that: CheckF[E, A])(implicit semigroup: Semigroup[E]): CheckF[E, A] =
			CheckF { a: A =>
				(this (a), that(a)) match {
					case (Left(e1), Left(e2)) => (e1 |+| e2).asLeft
					case (Left(e), Right(a)) => e.asLeft
					case (Right(a), Left(e)) => e.asLeft
					case (Right(r1), Right(r2)) => r1.asRight
				}
			}
	}

	val a: CheckF[List[String], Int] = CheckF { v =>
		if (v > 2) v.asRight
		else List("must be > 2").asLeft
	}

	val b: CheckF[List[String], Int] = CheckF { v =>
		if (v < -2) v.asRight
		else List("must be < -2").asLeft
	}

	val check = a and b

	println(check(1))

	val allOk: CheckF[Nothing, Int] = CheckF { v => v.asRight }

	// println(allOk and allOk)
}

import cats.syntax.apply._
import cats.syntax.validated._

object Ch10_v2 extends App {

	sealed trait Predicate[E, A] {
		def and(that: Predicate[E, A]): Predicate[E, A] = And(this, that)

		def or(that: Predicate[E, A]): Predicate[E, A] = Or(this, that)

		def apply(a: A)(implicit s: Semigroup[E]): Validated[E, A] =
			this match {
				case Pure(f) => f(a)
				case And(left, right) => (left(a), right(a)).mapN((_, _) => a)
				case Or(left, right) => left(a) match {
					case Valid(_) => a.valid
					case Invalid(e1) => right(a) match {
						case Valid(_) => a.valid
						case Invalid(e2) => (e1 |+| e2).invalid
					}
				}
			}
	}

	object Predicate {
		def lift[E, A](msg: E, cond: A => Boolean): Predicate[E, A] =
			Pure { a =>
				if (cond(a)) a.valid
				else msg.invalid
			}
	}

	final case class Pure[E, A](func: A => Validated[E, A]) extends Predicate[E, A]

	final case class And[E, A](left: Predicate[E, A], right: Predicate[E, A]) extends Predicate[E, A]

	final case class Or[E, A](left: Predicate[E, A], right: Predicate[E, A]) extends Predicate[E, A]


	sealed trait Check[E, A, B] {
		def apply(a: A)(implicit semigroup: Semigroup[E]): Validated[E, B]

		def map[C](func: B => C): Check[E, A, C] = MappedCheck(this, func)

		def flatMap[C](func: B => Check[E, A, C]): Check[E, A, C] = FlatMappedCheck[E, A, B, C](this, func)

		def andThen[C](that: Check[E, B, C]): Check[E, A, C] = AndThenCheck(this, that)
	}

	object Check {
		def apply[E, A](predicate: Predicate[E, A])(implicit semigroup: Semigroup[E]): Check[E, A, A] = PureCheck(predicate(_))

		def apply[E, A, B](f: A => Validated[E, B]): Check[E, A, B] = PureCheck(f)
	}

	final case class PureCheck[E, A, B](f: A => Validated[E, B]) extends Check[E, A, B] {
		override def apply(a: A)(implicit semigroup: Semigroup[E]): Validated[E, B] = f(a)
	}

	final case class MappedCheck[E, A, B, C](check: Check[E, A, B], func: B => C) extends Check[E, A, C] {
		override def apply(a: A)(implicit semigroup: Semigroup[E]): Validated[E, C] = check(a).map(func)
	}

	final case class FlatMappedCheck[E, A, B, C](check: Check[E, A, B], func: B => Check[E, A, C]) extends Check[E, A, C] {
		override def apply(a: A)(implicit semigroup: Semigroup[E]): Validated[E, C] =
			check(a).withEither(_.flatMap(b => func(b)(a).toEither))
	}

	final case class AndThenCheck[E, A, B, C](first: Check[E, A, B], second: Check[E, B, C]) extends Check[E, A, C] {
		override def apply(a: A)(implicit semigroup: Semigroup[E]): Validated[E, C] =
			first(a) match {
				case err@Invalid(_) => err
				case Valid(b) => second(b)
			}

		//			first(a).withEither(_.flatMap(b => second(b).toEither))
	}


	def error(s: String) = List(s)

	def longerThan(n: Int) = Predicate.lift(error(s"must be longer than ${n} characters"), (s: String) => s.length > n)

	val alphanumeric = Predicate.lift(error("must be all alphanumeric characters"), (s: String) => s.forall(_.isLetterOrDigit))

	def contains(c: Char) = Predicate.lift(error(s"must contain ${c} symbol"), (s: String) => s.contains(c))

	def containsOnce(c: Char) = Predicate.lift(error(s"must contain ${c} symbol exactly once"), (s: String) => s.count(_ == c) == 1)

	def checkUsername = Check(longerThan(2) and alphanumeric)


	val splitEmail: Check[List[String], String, (String, String)] = Check(containsOnce('@')).map { s =>
		val arr = s.split('@')
		(arr(0), arr(1))
	}

	val checkLeft: Check[List[String], String, String] = Check(longerThan(0))

	val checkRight: Check[List[String], String, String] = Check(longerThan(3) and contains('.'))

	val joinEmail: Check[List[String], (String, String), String] =
		Check { case (l, r) =>
			(checkLeft(l), checkRight(r)).mapN(_ + "@" + _)
		}

	val checkEmail: Check[List[String], String, String] = splitEmail andThen joinEmail

	final case class User(username: String, email: String)

	def createUser(username: String, email: String): Validated[List[String], User] =
		(checkUsername(username), checkEmail(email)).mapN(User)

	println(createUser("Noel", "noel@underscore.io"))
	println(createUser("", "dave@underscore@io"))
}

object Ch10_v3 extends App {

	import cats.data.Kleisli
	import cats.instances.list._

	val step1: Kleisli[List, Int, Int] = Kleisli(x => List(x + 1, x - 1))
	val step2: Kleisli[List, Int, Int] = Kleisli(x => List(x, x * 2))
	val pipeline = step1 andThen step2
	println(pipeline.run(2))


	sealed trait Predicate[E, A] {
		def and(that: Predicate[E, A]): Predicate[E, A] = And(this, that)

		def or(that: Predicate[E, A]): Predicate[E, A] = Or(this, that)

		def apply(a: A)(implicit s: Semigroup[E]): Validated[E, A] =
			this match {
				case Pure(f) => f(a)
				case And(left, right) => (left(a), right(a)).mapN((_, _) => a)
				case Or(left, right) => left(a) match {
					case Valid(_) => a.valid
					case Invalid(e1) => right(a) match {
						case Valid(_) => a.valid
						case Invalid(e2) => (e1 |+| e2).invalid
					}
				}
			}

		def run(implicit s: Semigroup[E]): A => Either[E, A] = apply(_).toEither
	}

	object Predicate {
		def lift[E, A](msg: E, cond: A => Boolean): Predicate[E, A] =
			Pure { a =>
				if (cond(a)) a.valid
				else msg.invalid
			}
	}

	final case class Pure[E, A](func: A => Validated[E, A]) extends Predicate[E, A]

	final case class And[E, A](left: Predicate[E, A], right: Predicate[E, A]) extends Predicate[E, A]

	final case class Or[E, A](left: Predicate[E, A], right: Predicate[E, A]) extends Predicate[E, A]

	type Errors = List[String]

	type Result[A] = Either[Errors, A]

	type Check[A, B] = Kleisli[Result, A, B]

	def check[A, B](func: A => Result[B]): Check[A, B] = Kleisli(func)

	def checkPred[A](pred: Predicate[Errors, A]) = Kleisli[Result, A, A](pred.run)

	def error(s: String): Errors = List(s)

	def longerThan(n: Int): Predicate[Errors, String] = Predicate.lift(error(s"must be longer than $n characters"), s => s.size > n)
}

