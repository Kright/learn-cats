package com.kright

import scala.language.higherKinds
import cats.Functor
import cats.instances.list._
import cats.instances.option._
import cats.syntax.functor._

object Ch3_4 extends App {

	val list = List(1, 2, 3)

	val list2 = Functor[List].map(list)(_ * 2)

	val option1 = Option(123)

	val option2 = Functor[Option].map(option1)(_.toString)

	def double[F[_] : Functor](start: F[Int]): F[Int] = start.map(_ * 2)

	println(double(List(1, 2, 3)))
	println(double(Option(3)))

	//	println(double(List(Option(3))))

	{
		sealed trait Tree[+A]

		final case class Branch[A](left: Tree[A], right: Tree[A]) extends Tree[A]

		final case class Leaf[A](value: A) extends Tree[A]

		object Tree {
			implicit val functor: Functor[Tree] = new Functor[Tree] {
				override def map[A, B](value: Tree[A])(f: A => B): Tree[B] =
					value match {
						case Leaf(v) => Leaf(f(v))
						case Branch(left, right) => Branch(map(left)(f), map(right)(f))
					}
			}

			def branch[A](left: Tree[A], right: Tree[A]): Tree[A] = Branch(left, right)

			def leaf[A](value: A): Tree[A] = Leaf(value)
		}

		val t = Tree.branch(Tree.leaf(2), Tree.leaf(3))
		println(t.map(_ * 2))
	}
}
