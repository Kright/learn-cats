package com.kright

import scala.language.higherKinds

import cats.syntax.functor._
import cats.instances.function._

object Ch4_1 extends App {

	trait Monad[F[_]] {
		def pure[A](a: A): F[A]

		def flatMap[A, B](value: F[A])(func: A => F[B]): F[B]

		def map[A, B](value: F[A])(func: A => B): F[B] =
			flatMap(value)(func map pure)
	}

}
