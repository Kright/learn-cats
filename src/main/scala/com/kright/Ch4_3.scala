package com.kright

import cats.syntax.either._

object Ch4_3 extends App {

	val a = 1.asRight[String]
	val b = 2.asRight[String]
	val c = "c".asLeft[Int]
	val d = "d".asLeft[Int]

	println(for {
		aa <- a
		bb <- b
		cc <- c
		dd <- d
	} yield aa + bb + cc + dd)

	println(Either.catchOnly[NumberFormatException]("foo".toInt))

	println(1.asLeft[String].recover{ case 1 => "1"})
}
