package com.kright

import cats.Monad
import cats.syntax.either._

import scala.annotation.tailrec

object Ch4_10 extends App {

	sealed trait Tree[+A]

	final case class Branch[A](left: Tree[A], right: Tree[A]) extends Tree[A]

	final case class Leaf[A](value: A) extends Tree[A]

	def branch[A](left: Tree[A], right: Tree[A]): Tree[A] = Branch(left, right)

	def leaf[A](value: A): Tree[A] = Leaf(value)

	implicit val treeMonad = new Monad[Tree] {
		override def flatMap[A, B](fa: Tree[A])(f: A => Tree[B]): Tree[B] = {
			fa match {
				case Branch(left, right) => branch(flatMap(left)(f), flatMap(right)(f))
				case Leaf(value) => f(value)
			}
		}

		//		override def tailRecM[A, B](a: A)(f: A => Tree[Either[A, B]]): Tree[B] =
		//			flatMap(f(a)) {
		//				case Left(va) => tailRecM(va)(f)
		//				case Right(vb) => leaf(vb)
		//			}

		override def tailRecM[A, B](a: A)(f: A => Tree[Either[A, B]]): Tree[B] = {
			@tailrec
			def loop(open: List[Tree[Either[A, B]]], closed: List[Option[Tree[B]]]): List[Tree[B]] =
				open match {
					case Branch(l, r) :: tail => loop(l :: r :: tail, None :: closed)
					case Leaf(Left(value)) :: tail => loop(f(value) :: tail, closed)
					case Leaf(Right(value)) :: tail => loop(tail, Some(pure(value)) :: closed)
					case Nil =>  // some black magic
						closed.foldLeft(Nil: List[Tree[B]]) { (acc, maybeTree) =>
							maybeTree.map(_ :: acc).getOrElse {
								val left :: right :: tail = acc
								branch(left, right) :: tail
							}
						}
				}

			loop(List(f(a)), Nil).head
		}

		override def pure[A](x: A): Tree[A] = leaf(x)
	}

}
