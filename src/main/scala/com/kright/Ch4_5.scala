package com.kright

import cats.MonadError


import cats.instances.either._


object Ch4_5 extends App {

	type ErrorOr[A] = Either[String, A]

	val monadError = MonadError[ErrorOr, String]

	val success = monadError.pure(3)
	val failure = monadError.raiseError("fatality")

	val r = monadError.handleError(failure) {
		case "fatality" => monadError.pure("ok")
		case _ => monadError.raiseError("error")
	}

	println(r)

	println(monadError.ensure(success)("should be positive")(_ > 0))

	{
		import cats.syntax.monadError._ // for ensure
		import cats.syntax.applicative._ // for pure
		import cats.syntax.applicativeError._

		val success = 42.pure[ErrorOr]

		println(success.ensure("non-positive")(_ > 0))

		val failure = "string".raiseError[ErrorOr, Int]

		println(failure)
	}
}
