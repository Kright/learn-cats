package com.kright

import cats.Eval

object Ch4_6 extends App {

	def getInt(i: Int): Int = {
		println(s"getInt($i)")
		i
	}

	def mul2(i: Int): Int = {
		println(s"mul2($i)")
		i * 2
	}

	val now = Eval.now(getInt(1))
	val later = Eval.later(getInt(2))
	val always = Eval.always(getInt(3))

	println("now")
	println(now.value)
	println(now.value)

	println("later")
	println(later.value)
	println(later.value)

	println("always")
	println(always.value)
	println(always.value)

	println("mul2")

	val now2 = now.map(mul2)
	val later2 = later.map(mul2)
	val always2 = always.map(mul2)

	println("now2")
	println(now2.value)
	println(now2.value)

	println("later2")
	println(later2.value)
	println(later2.value)

	println("always2")
	println(always2.value)
	println(always2.value)

	println("memoise")
	val mem = always2.memoize
	println(mem.value)
	println(mem.value)

	def factorial(n: BigInt): Eval[BigInt] =
		if (n == 1) {
			Eval.now(1)
		} else {
			Eval.defer(factorial(n - 1).map(_ * n))
		}

	println(factorial(50000).value)
}
