package com.kright

import cats.Semigroupal
import cats.data.Validated
import cats.syntax.either._
import cats.instances.list._

import scala.util.Try

object Ch6_4_4 extends App {

	case class User(name: String, age: Int)

	type FailFast[T] = Either[List[String], T]
	type FailsAll[T] = Validated[List[String], T]

	def getValue(name: String)(db: Map[String, String]): FailFast[String] =
		db.get(name).toRight(List(s"value for $name doesn't exist"))

	def parseInt(name: String)(value: String): FailFast[Int] =
		Either.catchOnly[NumberFormatException](value.toInt)
			.leftMap(_ => List(s"'$value' for '$name' should be an integer"))

	def nonBlank(value: String): FailFast[String] =
		value.asRight.ensure(List(s"value should be nonempty"))(_.nonEmpty)

	def nonNegative(name: String)(value: Int): FailFast[Int] =
		value.asRight.ensure(List(s"$name should be be non-negative"))(_ >= 0)


	def readName(db: Map[String, String]): Either[List[String], String] =
		getValue("name")(db)
			.flatMap(nonBlank)

	//		for {
	//			name <- getValue("name")(db)
	//			_ <- nonBlank(name)
	//		} yield name

	def readAge(db: Map[String, String]): Either[List[String], Int] =
		getValue("age")(db)
			.flatMap(parseInt("age"))
			.flatMap(nonNegative("age"))

	//		for {
	//			ageStr <- getValue("age")(db)
	//			int <- parseInt("age")(ageStr)
	//			_ <- nonNegative(int)
	//		} yield int

	import cats.instances.list._
	import cats.syntax.apply._

	def readUser(db: Map[String, String]): FailsAll[User] =
		(
			readName(db).toValidated,
			readAge(db).toValidated
		).mapN(User)

	println(readUser(Map("name" -> "name", "age" -> "3")))
	println(readUser(Map("name" -> "", "age" -> "3")))
	println(readUser(Map("name" -> "name", "age" -> "-3")))
	println(readUser(Map("name" -> "", "age" -> "-3")))
	println(readUser(Map()))

}
