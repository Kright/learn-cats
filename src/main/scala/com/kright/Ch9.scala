package com.kright

import cats.Monoid
import cats.syntax.monoid._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._

object Ch9 extends App {
	{
		def foldMap[A, B: Monoid](data: Vector[A])(f: A => B): B =
			data.map(f).foldLeft(Monoid[B].empty)(_ |+| _)

		def foldMap2[A, B: Monoid](data: Vector[A])(f: A => B): B =
			data.foldLeft(Monoid[B].empty)((b, a) => b |+| f(a))

		import cats.instances.int._

		println(foldMap(Vector(1, 2, 3))(identity))
		println(foldMap2(Vector(1, 2, 3))(identity))
	}

	{
		import cats.instances.future._

		def foldMap[A, B: Monoid](data: Vector[A])(f: A => B): B =
			data.foldLeft(Monoid[B].empty)((b, a) => b |+| f(a))

		def parallelFoldMap[A, B: Monoid](values: Vector[A])(func: A => B): Future[B] = {
			val processorsCount = Runtime.getRuntime.availableProcessors()
			val groups = values.grouped((values.size.toDouble / processorsCount).ceil.toInt)
			val groupResults = groups.map(data => Future(foldMap(data)(func)))
			Monoid[Future[B]].combineAll(groupResults)
		}

		import cats.instances.int._
		val fut = parallelFoldMap(Vector(1, 2, 3))(identity)
		println(Await.result(fut, 1.seconds))

		val result: Future[Int] = parallelFoldMap((1 to 1000000).toVector)(identity)

		println(Await.result(result, 1.seconds))
	}

	{
		import cats.instances.future._
		import cats.instances.vector._

		import cats.syntax.foldable._
		import cats.syntax.traverse._

		def parallelFoldMap[A, B: Monoid](values: Vector[A])(func: A => B): Future[B] = {
			val processorsCount = Runtime.getRuntime.availableProcessors()
			val groupSize = (values.size.toDouble / processorsCount).ceil.toInt

			values
				.grouped(groupSize)
				.toVector
				.traverse(part => Future(part.foldMap(func)))
				.map(_.combineAll)
		}

		import cats.instances.int._
		val result: Future[Int] = parallelFoldMap((1 to 1000000).toVector)(identity)
		println(Await.result(result, 1.seconds))
	}
}
