package com.kright

import cats.Monoid
import cats.instances.string._
import cats.instances.int._
import cats.instances.list._
import cats.instances.invariant._
import cats.instances.future._
import cats.syntax.apply._
import cats.syntax.semigroup._

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits._

object Ch6_2 extends App {

	case class Cat(name: String,
	               yearOfBirth: Int,
	               favoriteFoods: List[String])

	val tuple2cat: (String, Int, List[String]) => Cat = Cat

	val cat2tuple: Cat => (String, Int, List[String]) =
		cat => (cat.name, cat.yearOfBirth, cat.favoriteFoods)

	implicit val catMonoid: Monoid[Cat] = (
		Monoid[String],
		Monoid[Int],
		Monoid[List[String]]
	).imapN(tuple2cat)(cat2tuple)

	val gav = Cat("Gav", 1999, List("fish"))
	val garfield = Cat("Garfield", 1983, List("Lasagne"))

	println(gav |+| garfield)
	println(Monoid[Cat].empty)

	val futureCat = (
		Future("Garfield"),
		Future(1978),
		Future(List("Lasagne"))
	).mapN(Cat.apply)

	println(Await.result(futureCat, 1.second))
}
