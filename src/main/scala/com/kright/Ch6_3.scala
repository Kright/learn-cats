package com.kright

import cats.Semigroupal
import cats.instances.either._

object Ch6_3 extends App {

	type ErrorOr[A] = Either[Vector[String], A]

	val result = Semigroupal[ErrorOr].product(
		Left(Vector("Error 1")),
		Left(Vector("Error 2"))
	)

	println(result) // wtf?

	{
		import cats.syntax.semigroup._
		import cats.instances.vector._

		val semi = new Semigroupal[ErrorOr] {
			override def product[A, B](fa: ErrorOr[A], fb: ErrorOr[B]): ErrorOr[(A, B)] = {
				fa match {
					case Left(a) =>
						fb match {
							case Left(b) => Left(a |+| b)
							case Right(b) => Left(a)
						}
					case Right(a) =>
						fb match {
							case Left(b) => Left(b)
							case Right(b) => Right((a, b))
						}
				}
			}
		}

		println(semi.product(
			Left(Vector("error 1")),
			Left(Vector("error 2"))
		))
	}
}
