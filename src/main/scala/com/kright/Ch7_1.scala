package com.kright

import cats.{Eval, Monoid}
import cats.instances.int._

object Ch7_1 extends App {

	println(List(1, 2, 3).foldLeft(List.empty[Int])((lst, b) => b :: lst))
	println(List(1, 2, 3).foldRight(List.empty[Int]) { case (a, lst) => a :: lst })

	def map[T, U](lst: List[T])(f: T => U): List[U] =
		lst.foldRight(List.empty[U])((a, lst) => f(a) :: lst)

	def flatMap[T, U](lst: List[T])(f: T => List[U]): List[U] =
		lst.foldRight(List.empty[U]) { (a, lst) =>
			//			f(a) ++ lst
			//			f(a).foldRight(lst)((a, lst) => a :: lst)
			f(a) ::: lst
		}

	def filter[T](lst: List[T])(f: T => Boolean): List[T] =
		lst.foldRight(List.empty[T]) { (a, lst) =>
			if (f(a)) a :: lst else lst
		}

	def sum[T: Monoid](lst: List[T]): T =
		lst.foldRight(Monoid[T].empty)(Monoid[T].combine)

	def sum2[T: Numeric](lst: List[T]): T =
		lst.foldRight(implicitly[Numeric[T]].zero)(implicitly[Numeric[T]].plus)

	println(map(List(1, 20, 300))(_ * 2))

	println(flatMap(List(1, 20, 300))(i => List(i, i + 1)))

	println(filter(List(1, 2, 3, 4))(_ % 2 == 0))

	println(sum(List(1, 2, 3, 4, 5)))

	println(sum2(List(1, 2, 3, 4, 5)))

	{
		import cats.Foldable
		import cats.instances.list._
		import cats.instances.option._
		import cats.instances.stream._
		import cats.syntax.foldable._

		val ints = List(1, 2, 3)

		Foldable[List].foldLeft(ints, 0)(_ + _)

		println(Foldable[Option].foldLeft(Some(3), 10)(_ + _))
		println(Some(3).foldLeft(10)(_ + _))

		println((None: Option[Int]).foldLeft(10)(_ + _))

		def bigStream = (1 to 100000).toStream

		// println(bigStream.foldRight(0L)(_ + _))

		println(Foldable[Stream].foldRight(bigStream, Eval.now(0L))((num, eval) => eval.map(_ + num)).value)

		println(Foldable[List].find(List(1, 2, 3))(_ == 2))

		import cats.instances.string._
		println(Foldable[List].combineAll(List("a", "bb")))

		println(Foldable[List].foldMap(List(1, 2, 3))(_.toString))

		import cats.instances.vector._

		val listOfVectors = List(Vector(1, 2, 3), Vector(4, 5, 6))

		println((Foldable[List] compose Foldable[Vector]).combineAll(listOfVectors))

		import cats.syntax.foldable._

		println(List(1, 2, 3).combineAll)
	}
}
