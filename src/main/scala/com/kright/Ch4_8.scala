package com.kright

import cats.data.Reader

import cats.syntax.applicative._

object Ch4_8 extends App {

	case class Cat(name: String, favoriteFood: String)

	val catName: Reader[Cat, String] = Reader(cat => cat.name)

	def cat = Cat("gav", "sausage")

	println(catName(cat))
	println(catName.run(cat))

	val greetKitty: Reader[Cat, String] = catName.map(name => s"Hello, $name!")

	println(greetKitty.run(cat))

	val feedKitty = Reader[Cat, String](cat => s"feed with ${cat.favoriteFood}")

	val greetAndFeed = for {
		greet <- greetKitty
		feed <- feedKitty
	} yield s"$greet $feed"

	println(greetAndFeed.run(cat))

	{
		case class Db(usernames: Map[Int, String],
		              passwords: Map[String, String])

		type DbReader[T] = Reader[Db, T]

		def findUsername(id: Int): DbReader[Option[String]] =
			Reader(_.usernames.get(id))

		def checkPassword(username: String, password: String): DbReader[Boolean] =
			Reader(_.passwords.get(username).contains(password))

		def checkLogin(id: Int, password: String): DbReader[Boolean] =
			for {
				username <- findUsername(id)
				isOk <- username.map(checkPassword(_, password)).getOrElse(false.pure[DbReader])
			} yield isOk

		val users = Map(1 -> "dade", 2 -> "kate", 3 -> "margo")
		val passwords = Map("dade" -> "zerocool", "kate" -> "acidburn", "margo" -> "secret")
		val db = Db(users, passwords)

		println(checkLogin(1, "zerocool").run(db))
		println(checkLogin(4, "davinci").run(db))
	}
}
