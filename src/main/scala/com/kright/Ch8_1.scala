package com.kright

import cats.Eval

import scala.concurrent.{Await, Future}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

object Ch8_1 extends App {

	{
		import cats.syntax.traverse._
		import cats.instances.list._
		import cats.instances.future._

		trait UptimeClient {
			def getUptime(hostName: String): Future[Int]
		}

		class UptimeService(client: UptimeClient) {
			def getTotalUptime(hostNames: List[String]): Future[Int] =
				hostNames.traverse(client.getUptime).map(_.sum)
		}

		class TestUptimeClient(hosts: Map[String, Int]) extends UptimeClient {
			override def getUptime(hostName: String): Future[Int] =
				Future.successful[Int](hosts.getOrElse(hostName, 0))
		}

		import cats.syntax.eq._
		import cats.instances.int._

		def testTotalUptime() = {
			val hosts = Map("host1" -> 1, "host2" -> 20)
			val client = new TestUptimeClient(hosts)
			val service = new UptimeService(client)
			val actual = service.getTotalUptime(hosts.keys.toList)
			val expected = hosts.values.sum

			assert(Await.result(actual, 1.second) === expected)
		}

		testTotalUptime()
	}

	// better way
	{
		import cats.Id
		import cats.syntax.traverse._
		import cats.syntax.functor._
		import cats.Applicative
		import cats.instances.list._

		trait UptimeClient[F[_]] {
			def getUptime(hostName: String): F[Int]
		}

		trait RealUptimeClient extends UptimeClient[Future]

		trait TestUptimeClient extends UptimeClient[Id] {
			override def getUptime(hostName: String): Int // Id[Int] == Int :)
		}

		object TestUptimeClient {
			def apply(map: Map[String, Int]): TestUptimeClient = map.getOrElse(_, 0)
		}

		class UptimeService[F[_] : Applicative](client: UptimeClient[F]) {
			def getTotalUptime(hostNames: List[String]): F[Int] =
				hostNames.traverse(client.getUptime).map(_.sum)
		}

		import cats.syntax.eq._
		import cats.instances.int._

		def testTotalUptime() = {
			val hosts = Map("host1" -> 1, "host2" -> 20)
			val client = TestUptimeClient(hosts)
			val service = new UptimeService(client)
			val actual = service.getTotalUptime(hosts.keys.toList)
			val expected = hosts.values.sum

			assert(actual === expected)
		}

		testTotalUptime()
	}

	// better way
	{
		import cats.Id
		import cats.syntax.traverse._
		import cats.syntax.functor._
		import cats.Applicative
		import cats.instances.list._
		import cats.instances.int._
		import cats.syntax.foldable._

		import cats.Traverse

		trait UptimeClient[F[_]] {
			def getUptime(hostName: String): F[Int]
		}

		trait RealUptimeClient extends UptimeClient[Future]

		trait TestUptimeClient extends UptimeClient[Id] {
			override def getUptime(hostName: String): Int // Id[Int] == Int :)
		}

		object TestUptimeClient {
			def apply(map: Map[String, Int]): TestUptimeClient = map.getOrElse(_, 0)
		}

		class UptimeService[F[_] : Applicative](client: UptimeClient[F]) {
			def getTotalUptime[Lst[_] : Traverse](hostNames: Lst[String]): F[Int] = {
				hostNames.traverse(client.getUptime).map(_.combineAll)
			}
		}

		implicit val traverseForIterable = new Traverse[Iterable] {
			override def traverse[G[_], A, B](fa: Iterable[A])(f: A => G[B])(implicit evidence$1: Applicative[G]): G[Iterable[B]] =
				fa.toList.traverse(f).map(_.toIterable)

			override def foldLeft[A, B](fa: Iterable[A], b: B)(f: (B, A) => B): B = fa.foldLeft(b)(f)

			override def foldRight[A, B](fa: Iterable[A], lb: Eval[B])(f: (A, Eval[B]) => Eval[B]): Eval[B] =
				Eval.now[B](fa.foldRight(lb.value)((a, b) => f(a, Eval.now(b)).value))
		}

		import cats.syntax.eq._

		def testTotalUptime() = {
			val hosts = Map("host1" -> 1, "host2" -> 20)
			val client = TestUptimeClient(hosts)
			val service = new UptimeService(client)
			val actual = service.getTotalUptime(hosts.keys)
			val expected = hosts.values.sum

			assert(actual === expected)
		}

		testTotalUptime()
	}
}
