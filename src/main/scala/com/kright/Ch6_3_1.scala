package com.kright

import cats.Monad

import cats.syntax.flatMap._
import cats.syntax.functor._

object Ch6_3_1 extends App {
	def product[M[_] : Monad, A, B](x: M[A], y: M[B]): M[(A, B)] = {
		x.flatMap { xx =>
			y.map { yy =>
				(xx, yy)
			}
		}
	}

	def product2[M[_] : Monad, A, B](x: M[A], y: M[B]): M[(A, B)] =
		for {
			xx <- x
			yy <- y
		} yield (xx, yy)
}
