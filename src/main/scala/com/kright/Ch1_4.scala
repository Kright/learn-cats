package com.kright

import cats.Show
import cats.instances.int._
import cats.instances.string._
import cats.syntax.show._

object Ch1_4 extends App {

	val showInt = Show[Int]

	println(showInt.show(2))
	println(Show[String].show("string"))

	{
		import cats.syntax.show._
		println(42.show)
	}

	{
		implicit val catsStdShowForString: Show[String] = Show.show[String](s => s"'$s'")
		println(Show[String].show("bbb"))
	}

	final case class Cat(name: String, age: Int, color: String)

	object Cat {
		implicit val printableCat: Show[Cat] = cat => {
			import cats.instances.all._
			import cat._

			s"${name.show} is a ${age.show} year-old ${color.show} cat"
		}
	}

	println(Cat("_", 3, "white").show)
}
