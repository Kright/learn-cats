package com.kright

import cats.data.{EitherT, OptionT}
import cats.syntax.applicative._
import cats.instances.list._
import cats.instances.either._

import scala.concurrent.Future

object Ch5_1 extends App {

	type ListOption[A] = OptionT[List, A]

	val r1: ListOption[Int] = OptionT(List(Option(10), Option(3), None))
	val r2: ListOption[Int] = 32.pure[ListOption]

	val result = for {
		v1 <- r1
		v2 <- r2
	} yield v1 + v2

	r1.flatMap(v1 =>
		r2.map(v2 =>
			v1 + v2
		)
	)

	println(result)

	type ErrorOr[A] = Either[String, A]

	type ErrorOrOption[A] = OptionT[ErrorOr, A]

	val a = 10.pure[ErrorOrOption]
	val b = 32.pure[ErrorOrOption]

	val c = a.flatMap(x => b.map(y => x + y))
	println(c)


	type FutureEither[T] = EitherT[Future, String, T]

	type FutureEitherOption[T] = OptionT[FutureEither, T]

	val errorStack1 = OptionT[ErrorOr, Int](Right(Some(10)))
	val errorStack2 = 32.pure[ErrorOrOption]

	println(errorStack1.value)
	println(errorStack2.value.map(_.getOrElse(0)))
}
