package com.kright

object Ch3_6_2 extends App {

	trait Codec[A] {
		self =>

		def encode(value: A): String

		def decode(value: String): A

		def imap[B](dec: A => B, enc: B => A): Codec[B] = new Codec[B] {
			override def encode(value: B): String = self.encode(enc(value))

			override def decode(value: String): B = dec(self.decode(value))
		}
	}

	def encode[A: Codec](value: A): String = implicitly[Codec[A]].encode(value)

	def decode[A: Codec](value: String): A = implicitly[Codec[A]].decode(value)

	implicit val stringCodec: Codec[String] = new Codec[String] {
		override def encode(value: String): String = value

		override def decode(value: String): String = value
	}

	implicit val intCodec: Codec[Int] = stringCodec.imap(_.toInt, _.toString)

	implicit val doubleCodec: Codec[Double] = stringCodec.imap(_.toDouble, _.toString)

	println(doubleCodec.encode(12))
	assert(doubleCodec.decode(doubleCodec.encode(12)) == 12)

	final case class Box[A](value: A)

	object Box {
		implicit def boxCodec[T](implicit codecT: Codec[T]): Codec[Box[T]] = codecT.imap(Box(_), _.value)
	}

	println(encode(123.4))
	println(decode[Double]("123.4"))

	println(encode(Box(123.4)))
	println(decode[Box[Double]]("123.4"))
}
