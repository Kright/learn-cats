package com.kright

import cats.data.EitherT

import scala.concurrent.{Await, Future}
import cats.instances.future._
import scala.concurrent.duration._

import scala.concurrent.ExecutionContext.Implicits._

object Ch5_4 extends App {

	//	type Response[A] = Future[Either[String, A]]

	type FutureEither[A] = EitherT[Future, String, A]
	type Response[A] = FutureEither[A]

	val powerLevels = Map(
		"Jazz" -> 6,
		"Bumblebee" -> 8,
		"Hot Rod" -> 10
	)

	def getPowerLevel(autobot: String): Response[Int] =
		powerLevels.get(autobot) match {
			case Some(value) => EitherT.right(Future(value))
			case None => EitherT.left(Future(s"unknown name: ${autobot}"))
		}

	val t = getPowerLevel("a")

	def canSpecialMove(first: String, second: String): Response[Boolean] = {
		for {
			power1 <- getPowerLevel(first)
			power2 <- getPowerLevel(second)
		} yield (power1 + power2) > 15
	}

	def tacticalReport(first: String, second: String): String = {
		Await.result(canSpecialMove(first, second).value, 1.second) match {
			case Left(msg) => s"Error: $msg"
			case Right(true) => "can move"
			case Right(false) => "can't move"
		}
	}

	println(tacticalReport("Jazz", "Hot Rod"))
	println(tacticalReport("Jazz", "ee"))
}
