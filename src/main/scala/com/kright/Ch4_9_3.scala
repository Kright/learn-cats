package com.kright

import cats.data.State
import cats.syntax.applicative._

import scala.util.Try

object Ch4_9_3 extends App {

	type CalcState[A] = State[List[Int], A]

	object Operation {
		def unapply(sym: String): Option[(Int, Int) => Int] =
			sym match {
				case "+" => Option(_ + _)
				case "-" => Option(_ - _)
				case "*" => Option(_ * _)
				case "/" => Option(_ / _)
				case _ => None
			}
	}

	object Number {
		def unapply(sym: String): Option[Int] =
			Try(sym.toInt).toOption
	}

	def evalOne(sym: String): CalcState[Int] = State { stack =>
		sym match {
			case Number(num) =>
				(num :: stack, num)
			case Operation(op) =>
				stack match {
					case last :: prev :: lst =>
						val result = op(last, prev)
						(result :: lst, result)
				}
		}
	}

	def operand(num: Int): CalcState[Int] = State { stack => (num :: stack, num) }

	def operator(func: (Int, Int) => Int): CalcState[Int] = State {
		case last :: prev :: lst =>
			val result = func(last, prev)
			(result :: lst, result)
	}

	def evalOne2(sym: String): CalcState[Int] =
		sym match {
			case "+" => operator(_ + _)
			case "-" => operator(_ - _)
			case "*" => operator(_ * _)
			case "/" => operator(_ / _)
			case Number(num) => operand(num)
		}


	val result = for {
		_ <- evalOne2("1")
		_ <- evalOne("2")
		s3 <- evalOne("-")
	} yield s3


	def evalAll(list: List[String]): CalcState[Int] =
		list.foldLeft(0.pure[CalcState]) { (state, sym) =>
			state.flatMap(_ => evalOne2(sym))
		}

	println(result.run(List.empty).value)

	println(evalAll(List("1", "2", "+")).run(List.empty).value)

	def evalInput(code: String): Int = {
		val symbols = code.split(" ").filter(_.nonEmpty).toList
		evalAll(symbols).runA(List.empty).value
	}

	println(evalInput("1 2 + 2 2 + *"))
}
