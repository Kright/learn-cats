package com.kright

import cats.Monoid
import cats.syntax.semigroup._
import cats.instances.int._
import cats.instances.option._

object Ch2_5 extends App {

	def add[T: Monoid](items: List[T]): T = items.fold[T](Monoid[T].empty)(_ |+| _)

	println(add(List(1, 2, 3)))
	println(add(List(Option(1), Option(2))))

	case class Order(totalCost: Double, quantity: Double)

	object Order {
		implicit val monoid = new Monoid[Order] {
			override def empty: Order = Order(0, 0)

			override def combine(x: Order, y: Order): Order =
				Order(x.totalCost + y.totalCost, x.quantity + y.quantity)
		}
	}

	println(add(List(Option(Order(100500, 1)), None)))
}
