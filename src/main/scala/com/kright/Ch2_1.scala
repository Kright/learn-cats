package com.kright

import cats.{Monoid, Semigroup}
import cats.syntax.MonoidSyntax
import cats.syntax.semigroup._

object Ch2_1 extends App {

	object BoolMonoids {
		implicit val andMonoid = new Monoid[Boolean] {
			override def empty: Boolean = true

			override def combine(x: Boolean, y: Boolean): Boolean = x && y
		}

		implicit val orMonoid = new Monoid[Boolean] {
			override def empty: Boolean = false

			override def combine(x: Boolean, y: Boolean): Boolean = x || y
		}

		implicit val xorMonoid = new Monoid[Boolean] {
			override def empty: Boolean = false

			override def combine(x: Boolean, y: Boolean): Boolean = x != y
		}

		implicit val eqMonoid = new Monoid[Boolean] {
			override def empty: Boolean = true

			override def combine(x: Boolean, y: Boolean): Boolean = x == y
		}
	}

	object SetMonoids {
		implicit def unionMonoid[T] = new Monoid[Set[T]] {
			override def empty: Set[T] = Set.empty

			override def combine(x: Set[T], y: Set[T]): Set[T] = x union y
		}

		implicit def symmetricDiff[T] = new Monoid[Set[T]] {
			override def empty: Set[T] = Set.empty

			override def combine(x: Set[T], y: Set[T]): Set[T] = (x diff y) union (y diff x)
		}

		implicit def intersectionSemigroup[T]: Semigroup[Set[T]] =
			(x: Set[T], y: Set[T]) => x intersect y
	}

	{
		import BoolMonoids.andMonoid

		println(Monoid[Boolean].combine(true, false))
	}

	{
		import cats.instances.int._
		import cats.instances.option._

		println(Monoid.combine(Option(2), Option(10)))
		println(Monoid.combine(Option(2), None))
		println(Monoid.combine(Option(Monoid[Int].empty), None))

		val a = implicitly[Monoid[Option[Int]]]
	}

	{
		import cats.instances.string._

		println("yep, " |+| "combine me")
	}
}
