package com.kright

object Ch3_6_1 extends App {

	trait Printable[A] {
		self =>

		def format(value: A): String

		def contramap[B](func: B => A): Printable[B] = {
			value => self.format(func(value))
		}
	}

	def format[A](value: A)(implicit p: Printable[A]): String =
		p.format(value)

	implicit val stringPrintable: Printable[String] = s => "\"" + s + "\""

	implicit val booleanPrintable: Printable[Boolean] = if (_) "yes" else "no"

	final case class Box[A](value: A)

	object Box {
		implicit def printable[A](implicit printA: Printable[A]): Printable[Box[A]] =
			box => s"Box(${printA.format(box.value)})"
	}


	println(format("hello!"))
	println(format(true))

	println(format(Box("str")))
}
