package com.kright

import cats.Eq
import cats.syntax.eq._
import cats.instances.int._
import cats.instances.option._
import cats.syntax.option._

object Ch1_5 extends App {

	println(1 === 2)
	println(1 =!= 2)
	//	println(1 =!= "")

	val eqInt = Eq[Int]

	println(eqInt.eqv(1, 1))

	println(Option(1) =!= None)
	println((Some(1): Option[Int]) =!= None)
	println(Option(1) =!= None)

	println(1.some === none[Int])

	{
		import java.util.Date
		import cats.instances.long._

		implicit val dateEq: Eq[Date] = Eq.instance[Date](_.getTime === _.getTime)

		val (d1, d2) = (new Date(), new Date())
		println(d1 === d2)
	}

	{
		final case class Cat(name: String, age: Int, color: String)

		implicit val catEq = Eq.instance[Cat] { (cat1, cat2) =>
			import cats.instances.string._
			cat1.name === cat2.name && cat1.age === cat2.age && cat1.color === cat2.color
		}

		val cat1 = Cat("Garfield", 38, "orange and black")
		val cat2 = Cat("Garfield", 38, "orange and black")

		val optionCat1 = cat1.some
		val optionCat2 = cat2.some

		println(cat1 === cat2)
		println(optionCat1 === optionCat2)
//		println(cat1 === optionCat2)
	}
}
