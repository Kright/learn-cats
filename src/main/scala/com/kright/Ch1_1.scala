package com.kright

object Ch1_1 extends App {

	sealed trait Json

	final case class JsObject(get: Map[String, Json]) extends Json

	final case class JsString(get: String) extends Json

	final case class JsNumber(get: Double) extends Json

	case object JsNull extends Json

	trait JsonWriter[A] {
		def write(value: A): Json
	}

	case class Person(name: String, email: String)

	object JsonWriterInstances {
		implicit val stringWriter: JsonWriter[String] =
			(value: String) => JsString(value)

		implicit val personWriter: JsonWriter[Person] =
			(value: Person) => JsObject(Map("name" -> JsString(value.name), "email" -> JsString(value.email)))

		implicit def optionWriter[A](implicit writer: JsonWriter[A]): JsonWriter[Option[A]] = {
			case Some(value: A) => writer.write(value)
			case None => JsNull
		}
	}

	{
		object Json {
			def toJson[A](value: A)(implicit writer: JsonWriter[A]): Json =
				writer.write(value)
		}

		{
			import JsonWriterInstances._

			println(Json.toJson(Person("YepName", "ep@mail")))
		}
	}

	{
		object JsonSyntax {

			implicit class JsonWriterOps[A](value: A) {
				def toJson(implicit writer: JsonWriter[A]): Json = writer.write(value)
			}

		}

		import JsonWriterInstances._
		import JsonSyntax._

		println(Option(Person("YepName", "ep@mail")).toJson)
		println(implicitly[JsonWriter[Person]])
	}
}
