package com.kright

import cats.data.State

object Ch4_9 extends App {

	val step1 = State[Int, String] { state =>
		(state, s"The state is $state")
	}

	println(step1.run(10).value)
	println(step1.runS(10).value)
	println(step1.runA(10).value)

	val step2 = State[Int, String] { num =>
		(num * 2, s"state *=2")
	}

	val both = for {
		a <- step1
		b <- step2
	} yield (a, b)

	val both2 = for {
		state <- State.get[Int]
		msg1 <- State.pure[Int, String](s"The state is $state")
		_ <- State.set[Int](state * 2)
		msg2 <- State.pure[Int, String](s"state *=2")
	} yield (msg1, msg2)

	val both3 = for {
		msg1 <- State.inspect[Int, String] { state => s"The state is $state" }
		_ <- State.modify[Int](_ * 2)
		msg2 <- State.pure[Int, String](s"state *=2")
	} yield (msg1, msg2)

	val (s, r) = both3.run(1).value

	println(s, r)

	State.get[Int]
}
