package com.kright

import cats.{Foldable, Monoid}
import cats.kernel.CommutativeMonoid

object Ch11 extends App {

	{
		final case class GCounter(counters: Map[String, Int]) {
			def increment(machine: String, amount: Int) =
				GCounter(counters.updated(machine, amount + counters.getOrElse(machine, 0)))

			def merge(that: GCounter): GCounter =
				GCounter((counters.keySet ++ that.counters.keySet)
					.map(key => key -> (counters.getOrElse(key, 0) + that.counters.getOrElse(key, 0))).toMap)

			def total: Int = counters.values.sum
		}
	}

	trait BoundedSemiLattice[A] extends CommutativeMonoid[A] {
		def combine(a1: A, a2: A): A

		def empty: A
	}

	implicit val boundedSemiLatticeInts: BoundedSemiLattice[Int] = new BoundedSemiLattice[Int] {
		override def combine(a1: Int, a2: Int): Int = a1 max a2

		override def empty: Int = 0
	}

	implicit def boundedSemiLatticeSets[A]: BoundedSemiLattice[Set[A]] = new BoundedSemiLattice[Set[A]] {
		override def combine(a1: Set[A], a2: Set[A]): Set[A] = a1 union a2

		override def empty: Set[A] = Set.empty
	}

	import cats.syntax.semigroup._

	{
		final case class GCounter[A](counters: Map[String, A]) {
			def increment(machine: String, amount: A)(implicit m: CommutativeMonoid[A]): GCounter[A] = {
				GCounter(counters.updated(machine, amount |+| counters.getOrElse(machine, m.empty)))
			}

			def merge(that: GCounter[A])
			         (implicit bsl: BoundedSemiLattice[Set[String]], m: CommutativeMonoid[A]): GCounter[A] =
				GCounter(bsl.combine(counters.keySet, that.counters.keySet)
					.map(key => key -> (counters.getOrElse(key, m.empty) |+| that.counters.getOrElse(key, m.empty))).toMap)

			def total(implicit m: CommutativeMonoid[A]): A = m.combineAll(counters.values)
		}
	}

	{
		trait GCounter[F[_, _], K, V] {
			def increment(f: F[K, V])(k: K, v: V)(implicit m: CommutativeMonoid[V]): F[K, V]

			def merge(m1: F[K, V], m2: F[K, V])
			         (implicit bsl: BoundedSemiLattice[V]): F[K, V]

			def total(f: F[K, V])(implicit m: CommutativeMonoid[V])
		}

		object GCounter {
			def apply[F[_, _], K, V](implicit counter: GCounter[F, K, V]) = counter
		}

		implicit def mapCounter[K, V]: GCounter[Map, K, V] = new GCounter[Map, K, V] {
			override def increment(f: Map[K, V])(k: K, v: V)(implicit m: CommutativeMonoid[V]): Map[K, V] =
				f.updated(k, f.getOrElse(k, m.empty) |+| v)

			override def merge(m1: Map[K, V], m2: Map[K, V])(implicit bsl: BoundedSemiLattice[V]): Map[K, V] =
				(m1.keySet union m2.keySet).map(k => k -> (m1.getOrElse(k, bsl.empty) |+| m2.getOrElse(k, bsl.empty))).toMap

			override def total(f: Map[K, V])(implicit m: CommutativeMonoid[V]): Unit = m.combineAll(f.values)
		}
	}

	{
		trait KeyValueStore[F[_, _]] {
			def put[K, V](f: F[K, V])(k: K, v: V): F[K, V]

			def get[K, V](f: F[K, V])(k: K): Option[V]

			def getOrElse[K, V](f: F[K, V])(k: K, default: V): V = get(f)(k).getOrElse(default)

			def getOrEmpty[K, V](f: F[K, V])(k: K)(implicit m: Monoid[V]): V = getOrElse(f)(k, m.empty)

			def values[K, V](f: F[K, V]): Iterable[V]

			def keys[K, V](f: F[K, V]): Set[K]
		}

		implicit val kvStoreMap: KeyValueStore[Map] = new KeyValueStore[Map] {
			override def put[K, V](f: Map[K, V])(k: K, v: V): Map[K, V] = f.updated(k, v)

			override def get[K, V](f: Map[K, V])(k: K): Option[V] = f.get(k)

			override def values[K, V](f: Map[K, V]): Iterable[V] = f.values

			override def keys[K, V](f: Map[K, V]): Set[K] = f.keySet
		}

		trait GCounter[F[_, _], K, V] {
			def increment(f: F[K, V])(k: K, v: V)(implicit m: CommutativeMonoid[V], store: KeyValueStore[F]): F[K, V] =
				store.put(f)(k, v |+| store.getOrEmpty(f)(k))

			def merge(m1: F[K, V], m2: F[K, V])
			         (implicit bsl: BoundedSemiLattice[V], store: KeyValueStore[F]): F[K, V]


			def total(f: F[K, V])(implicit m: CommutativeMonoid[V], store: KeyValueStore[F]) = m.combineAll(store.values(f))
		}

		object GCounter {
			def apply[F[_, _], K, V](implicit counter: GCounter[F, K, V]) = counter
		}

		implicit def mapCounter[K, V]: GCounter[Map, K, V] = new GCounter[Map, K, V] {
			override def merge(m1: Map[K, V], m2: Map[K, V])(implicit bsl: BoundedSemiLattice[V], store: KeyValueStore[Map]): Map[K, V] =
				(m1.keySet union m2.keySet).map(k => k -> (store.getOrEmpty(m1)(k) |+| store.getOrEmpty(m2)(k))).toMap
		}
	}
}
