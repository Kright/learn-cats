package com.kright

import cats.Semigroupal
import cats.data.Validated
import cats.instances.list._

object Ch6_4 extends App {

	type AllErrorsOr[A] = Validated[List[String], A]

	val both = Semigroupal[AllErrorsOr].product(
		Validated.invalid(List("Error 1")),
		Validated.invalid(List("Error 2"))
	)

	println(both)

	println(Semigroupal[AllErrorsOr].product(Validated.valid("a"), Validated.valid("b")))
	println(Semigroupal[AllErrorsOr].product(Validated.valid("a"), Validated.invalid(List("error 2"))))

	{
		import cats.syntax.applicative._
		import cats.syntax.applicativeError._

		println(123.pure[AllErrorsOr])
		println(List("kek").raiseError[AllErrorsOr, Int])

		println(Validated.fromOption(None, "fail"))
	}

	{
		import cats.syntax.validated._
		import cats.syntax.apply._
		import cats.instances.string._

		println((
			"Error1".invalid[Int],
			"Err2".invalid[Int]
		).tupled)

		println("fail".invalid[Int].fold(_ + "!", _.toString))
	}
}
