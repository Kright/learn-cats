package com.kright

import scala.concurrent.{Future, Await}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

import cats._
import cats.implicits._

object Ch3_1 extends App {
	{
		val a = Left(1): Either[Int, String]
		val b = 1.asLeft[String]

		val future: Future[String] =
			Future(123)
				.map(n => n + 1)
				.map(_.toString)

		println(Await.result(future, 1.second))
	}

	{
		import cats.instances.function._
		import cats.syntax.functor._
		val int2d: Int => Double = _.toDouble

		val d2d: Double => Double = _ * 2

		println((int2d map d2d) (1))
		println((int2d andThen d2d) (1))
		println(d2d(int2d(1)))

		println(d2d.map(_ / 2).map(_.toInt)(1))
	}
}
