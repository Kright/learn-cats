package com.kright

import scala.language.higherKinds
import cats.Monad
import cats.instances.option._
import cats.instances.list._
import cats.syntax.applicative._
import cats.syntax.flatMap._
import cats.syntax.functor._
import cats.Id

import scala.annotation.tailrec

object Ch4_2 extends App {

	println(1.pure[Option])
	println(1.pure[List])

	def sumSquare[F[_] : Monad](a: F[Int], b: F[Int]): F[Int] =
	//		a.flatMap(x => b.map(y => x * x + y * y))
		for {
			x <- a
			y <- b
		} yield x * x + y * y


	println(sumSquare(Option(3), Option(4)))

	println(sumSquare(List(1, 2), List(10, 20)))

	println(sumSquare(3: Id[Int], Monad[Id].pure[Int](4)))

	val a: Id[Int] = Monad[Id].pure(3)
	val b = a.flatMap(_ + 1)
	println(b)

	val m = implicitly[Monad[Id]]
	println(m)

	{
		def pure[A](value: A): Id[A] = value

		def map[A, B](initial: Id[A])(func: A => B): Id[B] = func(initial)

		def flatMap[A, B](initial: Id[A])(func: A => Id[B]): Id[B] = func(initial)

		println(pure(3))
	}
}
