package com.kright

import cats.data.Writer
import cats.instances.vector._
import cats.syntax.writer._
import cats.syntax.applicative._


import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._

object Ch4_7 extends App {

	val w = Writer(Vector("log"), 12)

	println(w)

	val smth = 123.writer(Vector("msg1", "msg2"))
	println(smth)

	type Logged[A] = Writer[Vector[String], A]

	val writer1 = for {
		a <- 10.pure[Logged]
		_ <- Vector("kek").tell
		b <- 32.writer(Vector("lol"))
	} yield a + b

	println(writer1)

	println(writer1.value)
	println(writer1.written)

	val (log, result) = writer1.run
	println(log, result)

	def factorial(n: Int): Logged[Int] = {
		val ans = if (n == 0) {
			1.pure[Logged]
		} else {
			factorial(n - 1).map(_ * n)
		}
		ans.flatMap(i => i.writer(Vector(s"fact $n $i")))
	}

	def factorial2(n: Int): Logged[Int] =
		for {
			ans <- if (n == 0) {
				1.pure[Logged]
			} else {
				factorial(n - 1).map(_ * n)
			}
			_ <- Vector(s"fact $n $ans").tell
		} yield ans

	println(factorial(5))
	println(factorial2(5))

	val Vector(r1, r2) = Await.result(Future.sequence(Vector(
		Future(factorial(3).run),
		Future(factorial(3).run)
	)), 5.seconds)

	println(r1, r2)
}
