package com.kright

import cats.Semigroupal

import cats.instances.option._
import cats.syntax.apply._

object Ch6_1 extends App {

	println(Semigroupal[Option].product(Some(123), Some("text")))

	println(Semigroupal.tuple3(Option(1), Option(true), Option("yep")))

	println(Semigroupal.map2(Option(1), Option(2))(_ + _))

	case class Point(x: Int, y: Int)

	println(Semigroupal.map2(Option(1), Option(2))(Point))
	println((Option(1), Option(2)).mapN(Point))

	println((Option(123), Option("abc")).tupled)
}
