package com.kright

import scala.concurrent.{Await, Future}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

object Ch7_2 extends App {

	val hostNames = List("name", "anotherName")

	def getUptime(hostName: String): Future[Int] = Future(hostName.size)

	def allUptimes: Future[List[Int]] = Future.traverse(hostNames)(getUptime)

	println(Await.result(allUptimes, 1.second))
}
