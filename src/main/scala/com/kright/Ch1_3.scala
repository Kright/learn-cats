package com.kright

object Ch1_3 extends App {

	trait Printable[A] {
		def format(a: A): String
	}

	object PrintableInstances {
		implicit val printableInt: Printable[Int] = _.toString

		implicit val printableString: Printable[String] = s => s
	}

	object Printable {
		def format[A](a: A)(implicit p: Printable[A]): String = p.format(a)

		def print[A](a: A)(implicit p: Printable[A]): Unit = println(format(a))
	}

	{
		import PrintableInstances._
		Printable.print(1)
	}

	final case class Cat(name: String, age: Int, color: String)

	object Cat {
		implicit val printableCat: Printable[Cat] = cat => s"${cat.name} is a ${cat.age} year-old ${cat.color} cat"
	}

	Printable.print(Cat("Gav", 1, "black"))

	object PrintableSyntax {

		implicit class PrintableOps[A](a: A) {
			def format(implicit p: Printable[A]): String = p.format(a)

			def print(implicit p: Printable[A]): Unit = println(a.format)
		}

	}

	{
		import PrintableSyntax._
		Cat("Mia", 3, "red").print
	}
}
